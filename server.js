var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;

var path = require('path');
app.use(express.static(__dirname + '/build/default'));// Se usa para direccionar el contenido estatico
app.listen(port);

console.log('Todo list Frontal Polymer server started on: ' + port);

 
app.get('/', function(req, res){
    //res.send('Hola');
    res.sendFile("index.html", {root: '.'});
    //res.sendFile(path.join(__dirname, 'customersPinata.json'));
  });