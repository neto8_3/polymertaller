#Imagen Base
FROM node:latest

#Directorio de la app
WORKDIR /app

#copia de archivos
ADD ./build/default /app/build/default
ADD server.js /app
ADD package.json /app

#Dependecias
RUN npm install

#Puerto que expongo
EXPOSE 3001

#Comando
CMD ["npm","start"]